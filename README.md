
## Introduction

This repo demonstrates offline route calculation bug when HERE Android SDK Premium v3.12. Specifically, offline route calculation fails with `GRAPH_DISCONNEDTED` if using SCOOTER routing. These issues are not seen when using HERE SDK 3.9.

## Instructions

1. Place the HERE-sdk.aar in `app/libs` folder
2. Add HERE SDK credentials anywhere in strings.xml. Specifically, create 3 strings with ids `here_appid`, `here_apptoken` and `here_license_key` 
3. Run the app
4. Click on "Download Offline Map" button and wait for the toast that says it is complete (you can check the progress in LogCat)
5. Go offline (Airplane mode)
6. Click on "Calculate route" button.

### Expected result

The route is calculated (at least one result)

## Observed result

GRAPH_DISCONNECTED error

## What does the app do?

The app has 2 buttons:
1. Download offline maps: This button uses MapLoader APIs to download the map for Netherlands.
2. Calculate route: This button calculates the route between 2 hardcoded locations, withe SCOOTER routing.


