package com.etergo.heresdk.examples

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission.ACCESS_NETWORK_STATE
import android.Manifest.permission.ACCESS_WIFI_STATE
import android.Manifest.permission.INTERNET
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.here.android.mpa.common.ApplicationContext
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.common.MapEngine
import com.here.android.mpa.common.OnEngineInitListener
import com.here.android.mpa.common.Version
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.odml.MapLoader
import com.here.android.mpa.odml.MapPackage
import com.here.android.mpa.routing.CoreRouter
import com.here.android.mpa.routing.RouteOptions
import com.here.android.mpa.routing.RoutePlan
import com.here.android.mpa.routing.RouteResult
import com.here.android.mpa.routing.RouteWaypoint
import com.here.android.mpa.routing.Router
import com.here.android.mpa.routing.RoutingError
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var map: Map
    private val TAG = MainActivity::class.java.simpleName

    private val REQUEST_CODE_ASK_PERMISSIONS = 1
    private val RUNTIME_PERMISSIONS = arrayOf(
        ACCESS_FINE_LOCATION,
        WRITE_EXTERNAL_STORAGE,
        READ_EXTERNAL_STORAGE,
        INTERNET,
        ACCESS_WIFI_STATE,
        ACCESS_NETWORK_STATE
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d(TAG, "HERE SDK version: ${Version.getSdkVersion()}")

        btn_download.isEnabled = false
        btn_calculate_route.isEnabled = false

        btn_download.setOnClickListener { downloadNetherlandsMap() }
        btn_calculate_route.setOnClickListener { calculateRoute() }

        if (hasPermissions(this, *RUNTIME_PERMISSIONS)) {
            initHereSdk()
        } else {
            ActivityCompat
                .requestPermissions(this, RUNTIME_PERMISSIONS, REQUEST_CODE_ASK_PERMISSIONS);
        }

    }

    private fun hasPermissions(context: Context, vararg permissions: String): Boolean {
        permissions.forEach {
            if (ActivityCompat.checkSelfPermission(context, it)
                    != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_ASK_PERMISSIONS -> {
                permissions.forEachIndexed {index, permission ->
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {

                        if (!ActivityCompat
                                .shouldShowRequestPermissionRationale(this, permissions[index])) {
                            Toast.makeText(this, "Required permission " + permissions[index]
                                                   + " not granted. "
                                                   + "Please go to settings and turn on for sample app",
                                           Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(this, "Required permission " + permissions[index]
                                    + " not granted", Toast.LENGTH_LONG).show()
                        }
                    }
                }

                initHereSdk()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun initHereSdk() {
        MapEngine.getInstance().init(ApplicationContext(this)) {
            if (it == OnEngineInitListener.Error.NONE) {
                Log.d(TAG, "MapEngine init complete")
                map = Map()
                example_map_view.map = map
                btn_download.isEnabled = true
            } else {
                Log.d(TAG, "MapEngine init failed; error = $it, details = ${it.details}")
            }
        }
    }

    private fun calculateRoute() {
        val options = RouteOptions().apply {
            transportMode = RouteOptions.TransportMode.SCOOTER
            routeType = RouteOptions.Type.FASTEST
            routeCount = 1
        }

        val source = GeoCoordinate(52.352799999999995, 4.841178333333334)
        val destination = GeoCoordinate(52.327641, 4.87301)

        val routePlan = RoutePlan().apply {
            routeOptions = options
            addWaypoint(RouteWaypoint(source))
            addWaypoint(RouteWaypoint(destination))
        }

        val coreRouter = CoreRouter()
        coreRouter.calculateRoute(routePlan, object: Router.Listener<List<RouteResult>, RoutingError> {
            override fun onCalculateRouteFinished(routeResults: List<RouteResult>?, routingError: RoutingError?) {
                Log.d(TAG, "routeResults = $routeResults; routingError = $routingError")
                if(routingError != RoutingError.NONE) {
                    Toast.makeText(this@MainActivity, "Routing Error: $routingError", Toast.LENGTH_LONG).show()
                } else {
                    routeResults?.let {
                        Log.d(TAG, "Found ${it.size} routing results")
                    }
                }
            }

            override fun onProgress(progress: Int) {}

        })
    }

    private fun downloadNetherlandsMap() {
        val listener = object : MapLoader.Listener {
            override fun onCheckForUpdateComplete(
                p0: Boolean,
                p1: String?,
                p2: String?,
                p3: MapLoader.ResultCode?
            ) {
            }

            override fun onUninstallMapPackagesComplete(
                p0: MapPackage?,
                p1: MapLoader.ResultCode?
            ) {
            }

            override fun onPerformMapDataUpdateComplete(
                p0: MapPackage?,
                p1: MapLoader.ResultCode?
            ) {
            }

            override fun onProgress(progress: Int) {
                Log.d(TAG, "progress: $progress")
            }

            override fun onInstallationSize(p0: Long, p1: Long) {}

            override fun onInstallMapPackagesComplete(
                rootPackage: MapPackage?,
                resultCode: MapLoader.ResultCode?
            ) {
                Log.d(TAG, "Install map packages completed.")
                btn_calculate_route.isEnabled = true
                Toast.makeText(
                    this@MainActivity,
                    "Downloading maps completed. Now go offline and click on Calculate Route",
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onGetMapPackagesComplete(
                rootPackage: MapPackage?,
                resultCode: MapLoader.ResultCode?
            ) {
                if (rootPackage == null) {
                    Log.e(TAG, "rootPackage is null")
                    return
                }

                rootPackage.children.forEach { pkg ->
                    if(pkg.englishTitle.equals("Europe", ignoreCase = true)) {
                        val netherlands = pkg.children.filter {
                            it.englishTitle.equals("netherlands", ignoreCase = true)
                        }.firstOrNull()

                        if (netherlands != null) {
                            if (netherlands.installationState == MapPackage.InstallationState.INSTALLED) {
                                Log.d(TAG, "Netherlands map already installed")
                                btn_calculate_route.isEnabled = true
                                Toast.makeText(
                                    this@MainActivity,
                                    "Netherlands map already installed. Now go offline and click on Calculate Route",
                                    Toast.LENGTH_LONG
                                ).show()
                            } else {
                                Log.d(TAG, "Downloading Netherlands map")
                                MapLoader.getInstance().installMapPackages(listOf(netherlands.id))
                            }
                        }

                    }
                }



            }

        }

        MapLoader.getInstance().apply {
            addListener(listener)
            getMapPackages()
        }

    }

    override fun onResume() {
        super.onResume()
        example_map_view.onResume()
    }

    override fun onPause() {
        super.onPause()
        example_map_view.onPause()
    }
}
